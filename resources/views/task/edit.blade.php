@extends('layouts.app')

@section('content')
<form method="post"  action="{{action('TaskController@update', $task->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group">
   <label for="item">Update task</label>
   <input type="text" class="form-control" name="title" value="{{ $task->title}}" />
 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>



@endsection