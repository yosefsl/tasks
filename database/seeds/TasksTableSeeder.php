<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
           
             'title' => 'task 1',
             'status' => 'undone',
             'user_id' => 1,
             'created_at' => date('Y-m-d G:i:s')
            ], 
            [
              
                'title' => 'task 2',
                'status' => 'undone',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s')
               ],   
               [
                
                'title' => 'task 3',
                'status' => 'undone',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s')
               ],                      
        ]);      
    }
}
